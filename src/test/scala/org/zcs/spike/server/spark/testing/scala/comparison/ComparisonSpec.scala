package org.zcs.spike.server.spark.testing.scala.comparison

import org.apache.spark.sql.test.SharedSparkSession
import org.apache.spark.sql.{Encoders, QueryTest, Row}
import org.scalatest.matchers.should.Matchers
import org.zcs.spike.server.spark.testing.scala.model.Apple

class ComparisonSpec extends QueryTest with SharedSparkSession with Matchers {

  import testImplicits._

  testQuietly("One value") {
    val apple = Apple("Green", 85)
    val df = List(apple).toDF()
    val actual: Int = df.first.getAs("weight")

    actual shouldEqual apple.weight
  }

  testQuietly("Primitives list") {
    val df = List("Green", "Red").toDF("color")
    val actual = df.select("color").as(Encoders.STRING).collect()

    actual.size shouldEqual 2
  }

  testQuietly("One row as several primitives") {
    val expected = Apple("Green", 85)
    val df = List(expected).toDF()
    val Row(actualColor: String, actualWeight: Int) = df.first()
    Apple(actualColor, actualWeight) shouldEqual expected
  }

  testQuietly("DataFrames equals with 'checkAnswer'") {
    val apples = List(Apple("Green", 85))
    val expected = apples.toDF()
    val actual = apples.toDF()

    checkAnswer(expected, actual)
  }

  // Note: time-consuming
  testQuietly("DataFrames equals with 'except'") {
    val apples = List(Apple("Green", 85))
    val expected = apples.toDF()
    val actual = apples.toDF()

    actual.count() shouldEqual expected.count()
    actual.except(expected).count() shouldEqual 0
  }

  testQuietly("DataSets with collect") {
    val apples = List(Apple("Green", 85))
    val df = apples.toDF()

    val actual = df.as[Apple].collect()
    actual.toSet shouldEqual apples.toSet
  }


}
