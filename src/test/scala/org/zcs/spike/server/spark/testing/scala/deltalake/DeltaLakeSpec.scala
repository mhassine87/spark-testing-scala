package org.zcs.spike.server.spark.testing.scala.deltalake

import java.nio.file.Files
import com.typesafe.scalalogging.StrictLogging
import org.apache.spark.sql.functions._
import io.delta.tables.DeltaTable
import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, Encoders}
import org.apache.spark.sql.test.SharedSparkSessionBase
import org.scalatest.matchers.should.Matchers
import org.scalatest.BeforeAndAfter
import org.scalatest.freespec.AnyFreeSpec
import org.zcs.spike.server.spark.testing.scala.model.Apple


class DeltaLakeSpec extends AnyFreeSpec with SharedSparkSessionBase with Matchers with StrictLogging with BeforeAndAfter {
  val result = Files.createTempDirectory(getClass.getSimpleName).toFile
  result.deleteOnExit()

  val deltaTablePath = result.getPath

  import testImplicits._

  "When read existing delta Then read successfully" in {
    val df = read()
    df.show(false)
    val actual = df.as[Apple].collect()
    actual.toSet shouldEqual original.toSet
  }

  "When older Version specified Then previous data are read" in {
    logger.info("History table is:")
    spark
      .sql(s"DESCRIBE HISTORY delta.`$deltaTablePath`")
      .show(false)


    val deltaTable = DeltaTable.forPath(deltaTablePath)
    deltaTable.history().show(false)

    val df = spark
      .read
      .format("delta")
      .option("versionAsOf", 0)
      .load(deltaTablePath)
    df.show(false)
  }

  //    SELECT * FROM events VERSION AS OF version
  "When data deleted Then data absent after read" in {
    increment.toDS().write.mode("append")
      .format("delta")
      .option("userMetadata", "incorrect value is added")
      .save(deltaTablePath)
    read().count() shouldEqual increment.size + original.size

    val deltaTable = DeltaTable.forPath(deltaTablePath)
    deltaTable.delete(s"color=='${increment.head.color}'")

    val actual = read().as[Apple].collect()
    actual.toSet shouldEqual original.toSet
  }

  "When data is overwritten Then only overwritten data exists" in {
    increment.toDS().write.mode("append")
      .format("delta")
      .option("userMetadata", "data for overwrition is added")
      .save(deltaTablePath)
    read().count() shouldEqual increment.size + original.size

    val deltaTable = DeltaTable.forPath(deltaTablePath)
    val minimalVersion = deltaTable.history().select(min($"version")).as(Encoders.LONG).head()
    val originalDF = spark
      .read
      .format("delta")
      .option("versionAsOf", minimalVersion)
      .load(deltaTablePath)

    originalDF.write.mode("overwrite")
      .format("delta")
      .option("userMetadata", "original data restored")
      .save(deltaTablePath)

    read().count() shouldEqual original.size
  }


  def read(): DataFrame = {
    spark.read.format("delta")
      .load(deltaTablePath)
  }


  val original = List(Apple("green", 70), Apple("red", 110))
  val increment = List(Apple("blue", 350))

  def writeSampleData(): Unit = {
    original.toDS().write.format("delta").save(deltaTablePath)
  }

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    writeSampleData()
  }

  override protected def sparkConf: SparkConf = {
    val result = super.sparkConf
    result.set("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
    result.set("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
    result
  }

}
