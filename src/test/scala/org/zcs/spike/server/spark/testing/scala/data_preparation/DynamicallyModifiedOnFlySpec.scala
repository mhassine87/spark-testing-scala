package org.zcs.spike.server.spark.testing.scala.data_preparation

import java.io.File
import org.apache.spark.sql.QueryTest
import org.apache.spark.sql.execution.adaptive.{AdaptiveSparkPlanHelper, DisableAdaptiveExecutionSuite}
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.test.SharedSparkSession
import org.scalatest.matchers.should.Matchers
import org.zcs.spike.server.spark.testing.scala.repository.ApplesRepository

class DynamicallyModifiedOnFlySpec extends QueryTest with SharedSparkSession
  with AdaptiveSparkPlanHelper with DisableAdaptiveExecutionSuite with Matchers {

  val repository = new ApplesRepository

  test("Mass When weight specified Then weights sum") {
    val expected = 85
    val input = getBulkData.withColumn("weight", lit(85))
    repository.mass(input) shouldBe expected
  }

  private def getBulkData = spark.read.option("header", "true").csv(getTestDataFolder)

  private def getTestDataFolder = {
    val dataRoot = "src/test/test-data"
    val testSubPath = this.getClass.getName.replaceAll("\\.", "/")
    new File(dataRoot, testSubPath).getPath
  }


}
