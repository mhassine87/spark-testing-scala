package org.zcs.spike.server.spark.testing.scala.data_preparation

import org.apache.spark.sql.functions._
import org.apache.spark.sql.test.SharedSparkSession
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.{QueryTest, Row}
import org.scalatest.matchers.should.Matchers
import org.zcs.spike.server.spark.testing.scala.model.Apple

class DataFrameCreationSpec extends QueryTest with SharedSparkSession with Matchers {

  import testImplicits._

  test("Empty with schema") {
    val df = Seq.empty[(String, Int)].toDF("color", "weight")
    df.printSchema()
  }

  test("Empty with case class structure") {
    val df = Seq.empty[Apple].toDF()
    df.printSchema()
  }

  test("Empty with struct field") {
    val df = spark.emptyDataFrame
      .withColumn("apple", struct(lit("green") as "color", lit(110) as "weight"))
    df.printSchema()
  }

  test("From RDD") {
    val exampleString = "a b c d e f"
    val rdd = sparkContext.parallelize(Seq(exampleString))
      .map(_.split(" ").toSeq)
      .map(Row.fromSeq)
    val fields = (0 to 5).map(idx => StructField(name = "l" + idx, dataType = StringType, nullable = true))
    val df = spark.createDataFrame(rdd, StructType(fields))
    df.show(false)
  }

  test("Primitive list") {
    val df = List(1, 2, 3).toDF("values")
    df.show(false)
  }

  test("Tuples") {
    val df = List(("green", 70), ("red", 110)).toDF("color", "weight")
    df.show(false)
  }

  test("Array field") {
    val df = List(
      Array("red", "green", "yellow"),
      Array("green", "yellow")
    ).toDF()
    df.printSchema()
  }
  test("Struct field") {
    val schema = StructType(
      Seq(
        StructField(name = "id", dataType = IntegerType),
        StructField(name = "details", dataType = StructType(
          Seq(
            StructField(name = "name", dataType = StringType),
          )
        ))
      )
    )

    val row = Row.fromSeq(Seq(1, Row.fromSeq(Seq( "my name"))))
    import collection.JavaConverters._
    spark
      .createDataFrame(List(row).asJava, schema)
      .show(false)
  }

  test("With null values") {
    List(null.asInstanceOf[Integer]).toDF("color")
      .show(false)
  }

  test("Null for non-strings, where several value exists") {
    List(("green", null)).toDF("color", "weight")
      .withColumn("weight", $"weight" cast "Int")
      .show(false)
  }

  test("With null with Some") {
    val df = Seq(
      (None, Some(80)),
      (Some("green"), None)
    ).toDF("color", "weight")
    df.show(false)
  }

  test("Null values from select") {
    val df = spark.sql(s"SELECT current_timestamp() as dt, cast(null as string) as ss")
    df.show(false)
  }

}
