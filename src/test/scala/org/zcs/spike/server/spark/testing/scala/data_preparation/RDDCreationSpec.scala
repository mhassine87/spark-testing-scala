package org.zcs.spike.server.spark.testing.scala.data_preparation

import org.apache.spark.sql.QueryTest
import org.apache.spark.sql.test.SharedSparkSession
import org.scalatest.matchers.should.Matchers
import org.zcs.spike.server.spark.testing.scala.model.Apple

class RDDCreationSpec extends QueryTest with SharedSparkSession with Matchers {

  test("Empty RDD") {
    val rdd = sparkContext.emptyRDD[Apple]
    rdd.count() shouldBe 0
  }

  test("List of primitives") {
    val data = List(1, 2, 3)
    val rdd = sparkContext.parallelize(data)
    rdd.count shouldEqual data.size
  }

  test("List of entities") {
    val expected = Apple("Green", 120)
    val rdd = sparkContext.parallelize(List(expected))
    rdd.first() shouldEqual expected
  }

}
