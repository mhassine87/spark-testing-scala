package org.zcs.spike.server.spark.testing.scala.data_preparation

import org.apache.spark.sql.QueryTest
import org.apache.spark.sql.test.SharedSparkSession
import org.scalatest.matchers.should.Matchers
import org.zcs.spike.server.spark.testing.scala.model.Apple

class DataSetCreationSpec extends QueryTest with SharedSparkSession with Matchers {

  import testImplicits._

  test("Empty") {
    val df = Seq.empty[Apple].toDS()
    df.printSchema()
  }

  test("From list") {
    List(Apple("green", 70), Apple("red", 110)).toDS()
      .show(false)
  }

  test("From DataFrame") {
    List(("green", 70)).toDF("color", "weight")
      .as[Apple]
      .show(false)
  }

  test("Several fields has values") {
    List(createApple(weight = 60)).toDS
      .show(false)
  }

  def createApple(color: String = "Green",
                  weight: Int = 50
                  ): Apple = {
    Apple(color, weight)
  }


}
